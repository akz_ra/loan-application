package com.immobiler.immobilerexperience.service;

import com.immobiler.immobilerexperience.constant.CountryEnum;
import com.immobiler.immobilerexperience.exception.InvalidTenureException;
import com.immobiler.immobilerexperience.exception.LoanApplicationAmountException;
import com.immobiler.immobilerexperience.model.request.LoanApplicationRequest;
import com.immobiler.immobilerexperience.model.request.LoanRequestUpdate;
import com.immobiler.immobilerexperience.model.response.LoanResponse;
import com.immobiler.immobilerexperience.model.response.LoanResponseUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class LoanServiceUpdate {
    @Autowired
    LoanApplicationFeign loanApplicationFeign;

    public List<String> getList(String uuid, CountryEnum countryEnum) {
        return loanApplicationFeign.getTenure(countryEnum,uuid);

    }

    public LoanResponseUpdate createLoan(LoanRequestUpdate loanRequestUpdate) {
        if (loanRequestUpdate.getLoanAmount() > 599 || loanRequestUpdate.getLoanAmount() < 2001) {
            tenureCheck(loanRequestUpdate.getTenure());

            return loanApplicationFeign.loanOffers(loanRequestUpdate);
        } else {
            throw new LoanApplicationAmountException();
        }
    }

    public LoanResponse Loanconfirmation(LoanApplicationRequest loanApplicationRequest, CountryEnum countries, String uuid) {
        tenureCheck(loanApplicationRequest.getTenure());
        return loanApplicationFeign.confirmLoan(countries,uuid,loanApplicationRequest);
    }
































    public void tenureCheck(int tenure){

        if(tenure==12 || tenure==24  || tenure== 36 || tenure==48 || tenure==60){}
        else {
            throw new InvalidTenureException();
        }
    }
}
