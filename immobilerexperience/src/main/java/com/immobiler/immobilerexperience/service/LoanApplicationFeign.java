package com.immobiler.immobilerexperience.service;

import com.immobiler.immobilerexperience.constant.CountryEnum;
import com.immobiler.immobilerexperience.model.request.LoanApplicationRequest;
import com.immobiler.immobilerexperience.model.request.LoanRequest;
import com.immobiler.immobilerexperience.model.request.LoanRequestUpdate;
import com.immobiler.immobilerexperience.model.response.LoanApplicationResponse;
import com.immobiler.immobilerexperience.model.response.LoanApplicationUpdation;
import com.immobiler.immobilerexperience.model.response.LoanResponse;
import com.immobiler.immobilerexperience.model.response.LoanResponseUpdate;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "Loan-application-details", url = "${config.rest.service.getLoanUrl}")

public interface LoanApplicationFeign {
    @PostMapping("/loanapplication")
    public LoanApplicationResponse addLoan(@RequestBody LoanApplicationRequest loanApplicationRequest);

    @PutMapping("/updation")
    public LoanApplicationResponse modify(@RequestBody LoanApplicationUpdation applicationUpdation);

    @PutMapping("/confirmation")
    public LoanApplicationResponse loanApplicationConfirm(@RequestBody LoanRequest loanRequest);


    //updated
    @GetMapping("/tenureList")
    public List<String> getTenure(@RequestHeader("Countries") CountryEnum countries, @RequestHeader("uuid") String uuid);

    @PostMapping("/loan")
    public LoanResponseUpdate loanOffers (@RequestBody LoanRequestUpdate loanRequestUpdate);

    @PostMapping("/confirmation")
    public LoanResponse confirmLoan (@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid , @RequestBody LoanApplicationRequest loanApplicationRequest);





}


