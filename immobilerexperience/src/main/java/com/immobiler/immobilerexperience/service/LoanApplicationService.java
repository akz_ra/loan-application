package com.immobiler.immobilerexperience.service;

import com.immobiler.immobilerexperience.exception.LoanApplicationAmountException;
import com.immobiler.immobilerexperience.model.request.LoanApplicationRequest;
import com.immobiler.immobilerexperience.model.request.LoanRequest;
import com.immobiler.immobilerexperience.model.response.LoanApplicationResponse;
import com.immobiler.immobilerexperience.model.response.LoanApplicationUpdation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

@Service

public class LoanApplicationService {
    @Autowired
    LoanApplicationFeign loanApplicationFeign;

    public LoanApplicationResponse createApplication(LoanApplicationRequest loanApplicationRequest) {
        if (loanApplicationRequest.getLoanAmount() > 599 || loanApplicationRequest.getLoanAmount() < 2001) {
            return loanApplicationFeign.addLoan(loanApplicationRequest);
        } else {
            throw new LoanApplicationAmountException();
        }
    }

    public LoanApplicationResponse updateLoan(LoanApplicationUpdation applicationUpdation) {
        return loanApplicationFeign.modify(applicationUpdation);
    }
    public LoanApplicationResponse loanConfirmation(LoanRequest loanRequest) {
        return loanApplicationFeign.loanApplicationConfirm(loanRequest);
    }
}
