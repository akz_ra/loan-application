package com.immobiler.immobilerexperience.controller;

import com.immobiler.immobilerexperience.model.request.LoanApplicationRequest;
import com.immobiler.immobilerexperience.model.request.LoanRequest;
import com.immobiler.immobilerexperience.model.response.LoanApplicationResponse;
import com.immobiler.immobilerexperience.model.response.LoanApplicationUpdation;
import com.immobiler.immobilerexperience.service.LoanApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping
public class LoanApplicationController {
    @Autowired
    LoanApplicationService loanApplicationService;
    @PostMapping("/loan")
    public ResponseEntity<LoanApplicationResponse> create(@RequestBody LoanApplicationRequest loanApplicationRequest){
        return new ResponseEntity(loanApplicationService.createApplication(loanApplicationRequest), HttpStatus.OK);
    }
    @PutMapping("/loanupdation")
    public ResponseEntity<LoanApplicationResponse> update(@RequestBody LoanApplicationUpdation applicationUpdation){
        return new ResponseEntity(loanApplicationService.updateLoan(applicationUpdation), HttpStatus.OK);
    }
    @PutMapping("/loanconfirmation")
    public ResponseEntity<LoanApplicationResponse> loanConfirm(@RequestBody LoanRequest loanRequest){
        return new ResponseEntity(loanApplicationService.loanConfirmation(loanRequest), HttpStatus.OK);
    }
}
