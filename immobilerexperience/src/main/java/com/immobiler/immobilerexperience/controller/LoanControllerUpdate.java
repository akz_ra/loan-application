package com.immobiler.immobilerexperience.controller;

import com.immobiler.immobilerexperience.constant.CountryEnum;
import com.immobiler.immobilerexperience.exception.InvalidTenureException;
import com.immobiler.immobilerexperience.model.request.LoanApplicationRequest;
import com.immobiler.immobilerexperience.model.request.LoanRequestUpdate;
import com.immobiler.immobilerexperience.model.response.LoanResponse;
import com.immobiler.immobilerexperience.model.response.LoanResponseUpdate;
import com.immobiler.immobilerexperience.service.LoanServiceUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping
public class LoanControllerUpdate {
    @Autowired
    LoanServiceUpdate loanServiceUpdate;
//get tenure
    @GetMapping("/tenureCollection")
    public List<String> listTenure(@RequestHeader("Countries") CountryEnum countryEnum, @RequestHeader("uuid") String uuid) {
        return loanServiceUpdate.getList(uuid, countryEnum);
    }
// get loan details(loan amount,tenure)
    @PostMapping("/loanCreation")
    public ResponseEntity<LoanResponseUpdate> addLoan(@RequestBody @Valid LoanRequestUpdate loanRequestUpdate) {
        try {
            return new ResponseEntity(loanServiceUpdate.createLoan(loanRequestUpdate), HttpStatus.OK);
        } catch (InvalidTenureException i) {
            return new ResponseEntity(i.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }
//user confirmation
    @PostMapping("/loanconfirm")
    public ResponseEntity<LoanResponse> loanConfirm(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid, @RequestBody @Valid LoanApplicationRequest loanApplicationRequest) {
        try {

            return new ResponseEntity(loanServiceUpdate.Loanconfirmation(loanApplicationRequest, countries, uuid), HttpStatus.OK);
        } catch (InvalidTenureException i) {
            return new ResponseEntity(i.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }


}
