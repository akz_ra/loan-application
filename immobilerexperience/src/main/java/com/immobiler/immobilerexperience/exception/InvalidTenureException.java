package com.immobiler.immobilerexperience.exception;

public class InvalidTenureException extends RuntimeException{
    public InvalidTenureException(){
        super("Invalid tenure is given");
    }
}
