package com.immobiler.immobilerexperience.exception;

public class LoanApplicationAmountException extends RuntimeException{
    public LoanApplicationAmountException(){
        super("Loan Amount should be 600 JOD - 2000 JOD");
    }
}
