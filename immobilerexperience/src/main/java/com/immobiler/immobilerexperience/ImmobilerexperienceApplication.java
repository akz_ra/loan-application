package com.immobiler.immobilerexperience;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class ImmobilerexperienceApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImmobilerexperienceApplication.class, args);
	}

}
