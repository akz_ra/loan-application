package com.immobiler.immobilerexperience.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.swagger2.mappers.ModelMapper;

@Configuration
public class MapperConfiguration {
    @Bean
    public ModelMapper MapperConfiguration(){
        return new ModelMapper();
    }
}
