package com.immobiler.immobilerexperience.model.request;

import com.immobiler.immobilerexperience.constant.LoanApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequest {
    public int id;
    public LoanApplicationStatus loanApplicationStatus;
}
