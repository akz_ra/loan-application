package com.immobiler.immobilerexperience.model.request;

import com.immobiler.immobilerexperience.constant.SalaryRange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanApplicationRequest {
    public int id;
    @Size(min = 5 ,max = 30,message = "Enter Name with size between 5 & 30")
    public String name;
    public int age;
    @Pattern(regexp = "^\\d{10}$",message = "The National Id must be 10 digits")
    public String nationalId;
    @Min(value = 600,message = "Loan amout must be in the limit of 600 JOD to 2000 JOD")
    @Max(value = 2000,message = "Loan amout must be in the limit of 600 JOD to 2000 JOD")
    public float loanAmount;
    public int tenure;
    public SalaryRange salaryRange;
}
