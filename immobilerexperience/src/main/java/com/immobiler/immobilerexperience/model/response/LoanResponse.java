package com.immobiler.immobilerexperience.model.response;

import com.immobiler.immobilerexperience.constant.LoanApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanResponse {
    public int id;
    public LoanApplicationStatus loanApplicationStatus;
}
