package com.immobiler.immobilerexperience.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanApplicationResponse {
    public float installmentAmount;
    public float interestRate;
    public float totalInterestAmount;
    public float fees;
    public float totalAmount;
    public int tenure;
}
