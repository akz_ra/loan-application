package com.loanapplication.immobiler.repository;

import com.loanapplication.immobiler.model.entity.LoanApplicationEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface LoanApplicationRepository extends JpaRepository<LoanApplicationEntity,Integer> {
}
