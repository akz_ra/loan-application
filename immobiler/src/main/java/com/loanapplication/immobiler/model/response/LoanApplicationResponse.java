package com.loanapplication.immobiler.model.response;

import com.loanapplication.immobiler.constant.SalaryRange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class LoanApplicationResponse {
//    private int id;
    public float installmentAmount;
    public float interestRate;
    public float totalInterestAmount;
    public float fees;
    public float totalAmount;
    public int tenure;

}
