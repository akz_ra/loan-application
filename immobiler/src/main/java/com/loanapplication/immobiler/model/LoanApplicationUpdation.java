package com.loanapplication.immobiler.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class LoanApplicationUpdation {
    public int id;
    public int tenure;
    public float loanAmount;
}
