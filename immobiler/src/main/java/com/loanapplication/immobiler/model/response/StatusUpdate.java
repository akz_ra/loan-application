package com.loanapplication.immobiler.model.response;

import com.loanapplication.immobiler.constant.LoanApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StatusUpdate {
    public LoanApplicationStatus loanApplicationStatus;

}
