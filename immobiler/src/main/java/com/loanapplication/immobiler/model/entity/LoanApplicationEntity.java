package com.loanapplication.immobiler.model.entity;

import com.loanapplication.immobiler.constant.CountryEnum;
import com.loanapplication.immobiler.constant.LoanApplicationStatus;
import com.loanapplication.immobiler.constant.SalaryRange;
import jdk.jshell.Snippet;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "loan_application_table")

public class LoanApplicationEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="CustomerId")
    public int id;
    @Column(name = "CustomerName")
    public String name;
    @Column(name = "Age")
    public int age;
    @Column(name = "NationalId")
    public String nationalId;
    @Column(name = "LoanAmount")
    public float loanAmount;
    @Column(name = "InstallmentAmount")
    public float installmentAmount;
    @Column(name = "InterestRate")
    public float interestRate;
    @Column(name = "TotalInterestAmount")
    public float totalInterestAmount;
    @Column(name = "Fees")
    public float fees;
    @Column(name="TotalAmount")
    public float totalAmount;
    @Enumerated(EnumType.STRING)
    private SalaryRange salaryRange;
    @Enumerated(EnumType.STRING)
    public LoanApplicationStatus loanApplicationStatus;
    @Column(name = "Tenure")
    public int tenure;
    @CreatedDate
    @Column(name = "Loan_Request_created_date")
    private LocalDateTime createdDate;
    @CreatedBy
    private String createBy;
    @Column(name = "Loan_Request_uuid")
    private String uuid;
    @Column(name = "country")
    @Enumerated(EnumType.STRING)
    private CountryEnum countryEnum;


}
