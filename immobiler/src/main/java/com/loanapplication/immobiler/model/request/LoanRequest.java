package com.loanapplication.immobiler.model.request;

import com.loanapplication.immobiler.constant.LoanApplicationStatus;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequest {
    public int id;
    public LoanApplicationStatus loanApplicationStatus;
}
