package com.loanapplication.immobiler.model.request;

import com.loanapplication.immobiler.model.response.LoanResponseUpdate;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequestUpdate {
    public int tenure;
    public float loanAmount;
}
