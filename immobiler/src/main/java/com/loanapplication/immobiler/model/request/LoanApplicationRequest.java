package com.loanapplication.immobiler.model.request;

import com.loanapplication.immobiler.constant.SalaryRange;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor

public class LoanApplicationRequest {

    public String name;
    public int age;
    public String nationalId;
    public float loanAmount;
    public int tenure;
    public SalaryRange salaryRange;
}
