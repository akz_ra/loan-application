package com.loanapplication.immobiler.model;

import com.loanapplication.immobiler.model.request.LoanApplicationRequest;
import com.loanapplication.immobiler.model.response.LoanApplicationResponse;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Combined {
    private LoanApplicationRequest loanApplicationRequest;
    private LoanApplicationResponse loanApplicationResponse;
}
