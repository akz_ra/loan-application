package com.loanapplication.immobiler.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanResponseUpdate {
    public int tenure;
    public float loanAmount;
    public float installmentAmount;
    public float interestRate;
    public float totalInterestAmount;
    public float totalAmount;
    public float fee;
}
