package com.loanapplication.immobiler.constant;

public enum LoanApplicationStatus {
    PENDING("pending"),
    CONFIRMED("confirmed");

    private String value;
    LoanApplicationStatus(String value){
        this.value=value;
    }
}
