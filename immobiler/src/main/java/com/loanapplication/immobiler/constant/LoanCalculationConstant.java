package com.loanapplication.immobiler.constant;

public class LoanCalculationConstant {
    public static final float TWELVE_INTEREST_RATE = 8;
    public static final float TWELVE_INTEREST_FEES = 4;

    public static final float TWENTY_FOUR_INTEREST_RATE = 7.5f;
    public static final float TWENTY_FOUR_INTEREST_FEES = 3.5f;

    public static final float THIRTY_SIX_INTEREST_RATE = 7;
    public static final float THIRTY_SIX_INTEREST_FEES = 3;

    public static final float FOURTY_EIGHT_INTEREST_RATE = 6.5f;
    public static final float FOURTY_EIGHT_INTEREST_FEES = 2.5f;

    public static final float SIXTY_INTEREST_RATE = 6;
    public static final float SIXTY_INTEREST_FEES = 2;


}
