package com.loanapplication.immobiler.service;

import org.springframework.stereotype.Service;

@Service
public class LoanCalculation {
    int  calculateinstallment(float principal, float interestRate, float month) {
        float time = month / 12 ;
        interestRate=interestRate/(12*100);
        time=time*12;
        Double result= (principal*interestRate*Math.pow(1+interestRate,time))/(Math.pow(1+interestRate,time)-1);
        int emi = (int) Math.round( result);
        return emi;
    }

}
