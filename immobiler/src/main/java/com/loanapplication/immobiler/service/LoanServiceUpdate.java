package com.loanapplication.immobiler.service;

import com.loanapplication.immobiler.constant.CountryEnum;
import com.loanapplication.immobiler.constant.LoanApplicationStatus;
import com.loanapplication.immobiler.model.entity.LoanApplicationEntity;
import com.loanapplication.immobiler.model.request.LoanApplicationRequest;
import com.loanapplication.immobiler.model.request.LoanRequestUpdate;
import com.loanapplication.immobiler.model.response.LoanApplicationResponse;
import com.loanapplication.immobiler.model.response.LoanResponse;
import com.loanapplication.immobiler.model.response.LoanResponseUpdate;
import com.loanapplication.immobiler.model.response.StatusUpdate;
import com.loanapplication.immobiler.repository.LoanApplicationRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static com.loanapplication.immobiler.constant.LoanCalculationConstant.*;
import static com.loanapplication.immobiler.constant.LoanCalculationConstant.TWELVE_INTEREST_FEES;

@Service
public class LoanServiceUpdate {
    @Autowired
    LoanApplicationRepository loanApplicationRepository;
    @Autowired
            LoanCalculation loanCalculation;
    @Autowired
            ModelMapper modelMapper;


    List<Long> list = new ArrayList<>();
    public List<Long> getList() {
        list.add(12L);
        list.add(24L);
        list.add(36L);
        list.add(48L);
        return list;
    }


    public LoanResponseUpdate addLoanOffers(LoanRequestUpdate loanRequestUpdate) {
        float interestRate = 0;
        float fees = 0;
        switch (loanRequestUpdate.getTenure()) {
            case 60:
                interestRate = SIXTY_INTEREST_RATE;
                fees = SIXTY_INTEREST_FEES;
                break;
            case 48:
                interestRate = FOURTY_EIGHT_INTEREST_RATE;
                fees = FOURTY_EIGHT_INTEREST_FEES;
                break;
            case 36:
                interestRate = THIRTY_SIX_INTEREST_RATE;
                fees = THIRTY_SIX_INTEREST_FEES;
                break;
            case 24:
                interestRate = TWENTY_FOUR_INTEREST_RATE;
                fees = TWENTY_FOUR_INTEREST_FEES;
                break;
            case 12:
                interestRate = TWELVE_INTEREST_RATE;
                fees = TWELVE_INTEREST_FEES;
                break;
        }
        LoanApplicationEntity loanApplicationEntity= new LoanApplicationEntity();
        int installmentAmount = loanCalculation.calculateinstallment(loanRequestUpdate.getLoanAmount(), interestRate, loanRequestUpdate.getTenure());
        int totalAmount = installmentAmount * loanRequestUpdate.getTenure();
        int totalInterestAmount = (int) Math.round(totalAmount - loanRequestUpdate.getLoanAmount());
        LoanResponseUpdate loanResponseUpdate = new LoanResponseUpdate(loanRequestUpdate.getTenure(),loanRequestUpdate.getLoanAmount(),installmentAmount,interestRate,totalInterestAmount,fees,(totalAmount+fees));

//        loanApplicationRepository.save(loanApplicationEntity);
        return loanResponseUpdate;
    }

    public LoanResponse Loanconfirmation(LoanApplicationRequest loanApplicationRequest, CountryEnum countryEnum, String uuid) {
        float interestRate = 0;
        float fees = 0;
        switch (loanApplicationRequest.getTenure()){
            case 60:
                interestRate = SIXTY_INTEREST_RATE;
                fees = SIXTY_INTEREST_FEES;
                break;
            case 48:
                interestRate = FOURTY_EIGHT_INTEREST_RATE;
                fees = FOURTY_EIGHT_INTEREST_FEES;
                break;
            case 36:
                interestRate = THIRTY_SIX_INTEREST_RATE;
                fees = THIRTY_SIX_INTEREST_FEES;
                break;
            case 24:
                interestRate = TWENTY_FOUR_INTEREST_RATE;
                fees = TWENTY_FOUR_INTEREST_FEES;
                break;
            case 12:
                interestRate = TWELVE_INTEREST_RATE;
                fees = TWELVE_INTEREST_FEES;
                break;
        }

        int installmentAmount = loanCalculation.calculateinstallment(loanApplicationRequest.getLoanAmount(), interestRate, loanApplicationRequest.getTenure());
        int totalAmount = installmentAmount * loanApplicationRequest.getTenure();
        int totalInterestAmount = (int) Math.round(totalAmount - loanApplicationRequest.getLoanAmount());
        LoanApplicationEntity loanApplicationEntity = new LoanApplicationEntity();
        //


//        loanApplicationEntity.setId(null);

        loanApplicationEntity.setName(loanApplicationRequest.getName());
        loanApplicationEntity.setAge(loanApplicationRequest.getAge());
        loanApplicationEntity.setNationalId(loanApplicationRequest.getNationalId());
        loanApplicationEntity.setLoanAmount(loanApplicationRequest.getLoanAmount());
        loanApplicationEntity.setTenure(loanApplicationRequest.getTenure());
        loanApplicationEntity.setSalaryRange(loanApplicationRequest.getSalaryRange());
        loanApplicationEntity.setInstallmentAmount(installmentAmount);
        loanApplicationEntity.setInterestRate(interestRate);
        loanApplicationEntity.setTotalInterestAmount(totalInterestAmount);
        loanApplicationEntity.setFees(fees);
        loanApplicationEntity.setTotalAmount(totalAmount);

        loanApplicationEntity.setLoanApplicationStatus(LoanApplicationStatus.CONFIRMED);

        loanApplicationEntity.setUuid(uuid);
        loanApplicationEntity.setCountryEnum(countryEnum);

        loanApplicationEntity.setCreateBy(loanApplicationRequest.getName());

        loanApplicationEntity.setCreatedDate(LocalDateTime.now());

     LoanApplicationEntity response =   loanApplicationRepository.save(loanApplicationEntity);
return new LoanResponse(response.getId(),response.loanApplicationStatus);
    }
}
