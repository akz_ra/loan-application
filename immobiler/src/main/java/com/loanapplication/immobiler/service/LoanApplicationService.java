package com.loanapplication.immobiler.service;

import com.loanapplication.immobiler.constant.LoanApplicationStatus;
import com.loanapplication.immobiler.constant.SalaryRange;
import com.loanapplication.immobiler.model.LoanApplicationUpdation;
import com.loanapplication.immobiler.model.entity.LoanApplicationEntity;
import com.loanapplication.immobiler.model.request.LoanApplicationRequest;
import com.loanapplication.immobiler.model.request.LoanRequest;
import com.loanapplication.immobiler.model.response.LoanApplicationResponse;
import com.loanapplication.immobiler.model.response.LoanResponse;
import com.loanapplication.immobiler.repository.LoanApplicationRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static com.loanapplication.immobiler.constant.LoanCalculationConstant.*;

@Service

public class LoanApplicationService {
    @Autowired
    LoanApplicationRepository repository;
    @Autowired
    ModelMapper mapper;
    @Autowired
    LoanCalculation loanCalculation;
    public LoanApplicationResponse add(LoanApplicationRequest loanApplicationRequest) {
        float interestRate = 0;
        float fees = 0;
        switch (loanApplicationRequest.getTenure()){
            case 60:
                interestRate = SIXTY_INTEREST_RATE;
                fees = SIXTY_INTEREST_FEES;
                break;
            case 48:
                interestRate = FOURTY_EIGHT_INTEREST_RATE;
                fees = FOURTY_EIGHT_INTEREST_FEES;
                break;
            case 36:
                interestRate = THIRTY_SIX_INTEREST_RATE;
                fees = THIRTY_SIX_INTEREST_FEES;
                break;
            case 24:
                interestRate = TWENTY_FOUR_INTEREST_RATE;
                fees = TWENTY_FOUR_INTEREST_FEES;
                break;
            case 12:
                interestRate = TWELVE_INTEREST_RATE;
                fees = TWELVE_INTEREST_FEES;
                break;
        }
        int installmentAmount = loanCalculation.calculateinstallment(loanApplicationRequest.getLoanAmount(),interestRate,loanApplicationRequest.getTenure());
        int totalAmount = installmentAmount * loanApplicationRequest.getTenure();
        int totalInterestAmount = (int) Math.round(totalAmount-loanApplicationRequest.getLoanAmount());
        LoanApplicationResponse loanApplicationResponse = new LoanApplicationResponse(installmentAmount,interestRate,totalInterestAmount,fees,(totalAmount+fees), loanApplicationRequest.tenure);
        LoanApplicationEntity loanApplicationEntity=new LoanApplicationEntity();
//        loanApplicationEntity.setId(loanApplicationRequest.getId());
        loanApplicationEntity.setName(loanApplicationRequest.getName());
        loanApplicationEntity.setAge(loanApplicationRequest.getAge());
        loanApplicationEntity.setNationalId(loanApplicationRequest.getNationalId());
        loanApplicationEntity.setLoanAmount(loanApplicationRequest.getLoanAmount());
        loanApplicationEntity.setTenure(loanApplicationRequest.getTenure());
        loanApplicationEntity.setInstallmentAmount(loanApplicationResponse.getInstallmentAmount());
        loanApplicationEntity.setInterestRate(loanApplicationResponse.getInterestRate());
        loanApplicationEntity.setTotalInterestAmount(loanApplicationResponse.getTotalInterestAmount());
        loanApplicationEntity.setFees(loanApplicationResponse.getFees());
        loanApplicationEntity.setTotalAmount(loanApplicationResponse.getTotalAmount());
        loanApplicationEntity.setSalaryRange(SalaryRange.valueOf(loanApplicationRequest.getSalaryRange().name()));
        loanApplicationEntity.setLoanApplicationStatus(LoanApplicationStatus.PENDING);
        repository.save(loanApplicationEntity);
        return loanApplicationResponse;
    }

    public LoanApplicationResponse updateApplication(LoanApplicationUpdation applicationUpdation) {
        float interestRate = 0;
        float fees = 0;
        switch (applicationUpdation.getTenure()) {
            case 60:
                interestRate = SIXTY_INTEREST_RATE;
                fees = SIXTY_INTEREST_FEES;
                break;
            case 48:
                interestRate = FOURTY_EIGHT_INTEREST_RATE;
                fees = FOURTY_EIGHT_INTEREST_FEES;
                break;
            case 36:
                interestRate = THIRTY_SIX_INTEREST_RATE;
                fees = THIRTY_SIX_INTEREST_FEES;
                break;
            case 24:
                interestRate = TWENTY_FOUR_INTEREST_RATE;
                fees = TWENTY_FOUR_INTEREST_FEES;
                break;
            case 12:
                interestRate = TWELVE_INTEREST_RATE;
                fees = TWELVE_INTEREST_FEES;
                break;
        }
        LoanApplicationEntity loanApplicationEntity=repository.getReferenceById(applicationUpdation.getId());
        int installmentAmount = loanCalculation.calculateinstallment(applicationUpdation.getLoanAmount(), interestRate, applicationUpdation.getTenure());
        int totalAmount = installmentAmount * applicationUpdation.getTenure();
        int totalInterestAmount = (int) Math.round(totalAmount - applicationUpdation.getLoanAmount());
        LoanApplicationResponse loanApplicationResponse = new LoanApplicationResponse(installmentAmount,interestRate,totalInterestAmount,fees,(totalAmount+fees),applicationUpdation.getTenure() );
        loanApplicationEntity.setInstallmentAmount(installmentAmount);
        loanApplicationEntity.setInterestRate(interestRate);
        loanApplicationEntity.setTotalAmount(totalAmount);
        loanApplicationEntity.setFees(fees);
        loanApplicationEntity.setTotalInterestAmount(totalInterestAmount);
        loanApplicationEntity.setTenure(applicationUpdation.getTenure());
        repository.save(loanApplicationEntity);
        return loanApplicationResponse;
    }

    public LoanResponse confirmation(LoanRequest loanRequest) {
        LoanApplicationEntity loanApplicationEntity=repository.getReferenceById(loanRequest.getId());
        LoanResponse loanResponse=new LoanResponse(loanApplicationEntity.getId(),loanRequest.getLoanApplicationStatus());
        loanApplicationEntity.setLoanApplicationStatus(loanRequest.getLoanApplicationStatus());
        repository.save(loanApplicationEntity);
        return loanResponse;

    }
}
