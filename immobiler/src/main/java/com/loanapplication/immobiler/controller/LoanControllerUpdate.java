package com.loanapplication.immobiler.controller;

import com.loanapplication.immobiler.constant.CountryEnum;
import com.loanapplication.immobiler.model.request.LoanApplicationRequest;
import com.loanapplication.immobiler.model.request.LoanRequest;
import com.loanapplication.immobiler.model.request.LoanRequestUpdate;
import com.loanapplication.immobiler.model.response.LoanApplicationResponse;
import com.loanapplication.immobiler.model.response.LoanResponse;
import com.loanapplication.immobiler.model.response.LoanResponseUpdate;
import com.loanapplication.immobiler.model.response.StatusUpdate;
import com.loanapplication.immobiler.service.LoanServiceUpdate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping
public class LoanControllerUpdate {
    @Autowired
    LoanServiceUpdate loanServiceUpdate;

    @GetMapping("/tenureList")
    public List<Long> listTenure(@RequestHeader("Countries")CountryEnum countryEnum,String uuid){
        return loanServiceUpdate.getList();
    }
    @PostMapping("/loan")
    public ResponseEntity<LoanResponseUpdate> addLoan(@RequestBody LoanRequestUpdate loanRequestUpdate){
     return new ResponseEntity(loanServiceUpdate.addLoanOffers(loanRequestUpdate), HttpStatus.OK);
    }
    @PostMapping("/confirmation")
    public ResponseEntity<LoanResponse> loanConfirm(@RequestHeader("country") CountryEnum countries, @RequestHeader("uuid") String uuid , @RequestBody LoanApplicationRequest loanApplicationRequest){
        return new ResponseEntity(loanServiceUpdate.Loanconfirmation(loanApplicationRequest,countries,uuid), HttpStatus.OK);
    }
}
