package com.loanapplication.immobiler.controller;

import com.loanapplication.immobiler.model.LoanApplicationUpdation;
import com.loanapplication.immobiler.model.request.LoanApplicationRequest;
import com.loanapplication.immobiler.model.request.LoanRequest;
import com.loanapplication.immobiler.model.response.LoanApplicationResponse;
import com.loanapplication.immobiler.service.LoanApplicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping

public class LoanApplicationController {
    @Autowired
    LoanApplicationService service;
    @PostMapping("/loanapplication")
    public ResponseEntity<LoanApplicationResponse> add(@RequestBody LoanApplicationRequest loanApplicationRequest){
        return new ResponseEntity(service.add(loanApplicationRequest), HttpStatus.OK);
    }
    @PutMapping("/updation")
    public ResponseEntity<LoanApplicationResponse> update(@RequestBody LoanApplicationUpdation applicationUpdation){
        return new ResponseEntity(service.updateApplication(applicationUpdation), HttpStatus.OK);
    }
    @PutMapping("/confirmatio")
    public ResponseEntity<LoanApplicationResponse> confirm(@RequestBody LoanRequest loanRequest){
        return new ResponseEntity(service.confirmation(loanRequest), HttpStatus.OK);
    }

}
