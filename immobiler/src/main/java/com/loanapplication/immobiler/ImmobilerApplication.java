package com.loanapplication.immobiler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImmobilerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ImmobilerApplication.class, args);
	}

}
